using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace InsideEdge.Android.Fragments
{
    public class ApplicationDetailFragment : Fragment
    {
        public ApplicationDetailFragment()
        {
            RetainInstance = true;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.appDescription, null);

            //var grid = view.FindViewById<GridView>(Resource.Id.grid);
            //_friends = Util.GenerateApps();
            //grid.Adapter = new DashboardAdapter(Activity, _friends);
            //grid.ItemClick += GridOnItemClick;
            var submitButton = (Button) view.FindViewById(Resource.Id.submitBtn);
            //submitButton.Click += (o, e) => {
            //};
            submitButton.Click += delegate { submitButton.Text = "Something"; };
            return view;
        }
    }
}