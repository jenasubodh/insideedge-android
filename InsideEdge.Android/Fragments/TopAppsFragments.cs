using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.Activities;
using InsideEdge.Android.Adapters;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Fragments
{
    internal class TopAppsFragments : Fragment
    {
        private List<ApplicationViewModel> _apps;

        public TopAppsFragments()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.applist_fragment, null);

            var listView = view.FindViewById<ListView>(Resource.Id.app_List);
            _apps = Util.GenerateApps();
            _apps.Reverse();

            listView.Adapter = new AppListAdapter(Activity, _apps);
            listView.ItemClick += ListOnItemClick;

            return view;
        }

        private void ListOnItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var intent = new Intent(Activity, typeof (ApplicationDetailActivity));
            intent.PutExtra("Id", _apps[e.Position].Id.ToString());
            //intent.PutExtra("Image", _apps[itemClickEventArgs.Position].Image);
            StartActivity(intent);
        }
    }
}