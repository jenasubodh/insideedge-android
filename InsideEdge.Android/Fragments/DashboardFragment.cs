using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace InsideEdge.Android.Fragments
{
    public class DashboardFragment : Fragment
    {
        public DashboardFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.dashboard, null);

            var fragment1 = FragmentManager.FindFragmentById(Resource.Id.recentApps_fragment);
            TextView textView = (TextView)fragment1.View.FindViewById(Resource.Id.textView1);
            textView.Text = "Recent";

            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.main_activity_actions, menu);
        }
    }
}