using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Views;
using DK.Ostebaronen.Droid.ViewPagerIndicator;
using InsideEdge.Android.Adapters;

namespace InsideEdge.Android.Fragments
{
    public class AppViewPagerFragment : Fragment
    {
        private TitlePageIndicator _mPageIndicator;
        private FragmentPagerAdapter _mAdapter;
        private ViewPager _mViewPager;

        public AppViewPagerFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.app_fragment, null);

            // Create your application here
            _mViewPager = view.FindViewById<ViewPager>(Resource.Id.viewPager);
            _mViewPager.OffscreenPageLimit = 4;
            this._mPageIndicator = view.FindViewById<TitlePageIndicator>(Resource.Id.viewPagerIndicator);

            //Since we are a fragment in a fragment you need to pass down the child fragment manager!
            _mAdapter = new AppsViewPagerAdapter(ChildFragmentManager);

            var density = Resources.DisplayMetrics.Density;
            _mViewPager.Adapter = _mAdapter;
            this._mPageIndicator.SetViewPager(this._mViewPager);
            _mPageIndicator.FooterIndicatorStyle = TitlePageIndicator.IndicatorStyle.Triangle;
            this._mPageIndicator.CurrentItem = 1;

            //this._mPageIndicator.SetBackgroundColor(Color.Argb(0x18, 0xFF, 0x00, 0x00));
            //this._mPageIndicator.FooterColor = Color.Argb(0xFF, 0xAA, 0x22, 0x22);
            this._mPageIndicator.FooterColor = Color.Rgb(233, 106,21);
            this._mPageIndicator.FooterLineHeight = 1 * density;
            this._mPageIndicator.FooterIndicatorHeight = 3 * density;
            this._mPageIndicator.FooterIndicatorStyle = TitlePageIndicator.IndicatorStyle.Underline;

            this._mPageIndicator.TextColor = Color.Rgb(0, 51, 102);
            this._mPageIndicator.SelectedColor = Color.Rgb(0, 51, 102);
            this._mPageIndicator.IsSelectedBold = true;
            return view;
        }
    }
}