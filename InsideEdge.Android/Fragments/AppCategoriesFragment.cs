using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.Activities;
using InsideEdge.Android.Adapters;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Fragments
{
    public class AppCategoriesFragment : Fragment
    {
        private List<CategoriesViewModel> _apps;

        public AppCategoriesFragment()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.app_categories_fragment, null);
            var listView = view.FindViewById<ListView>(Resource.Id.app_CategoryList);
            _apps = Util.GenerateCategories();
            _apps.Reverse();

            listView.Adapter = new AppCategoriesAdapter(Activity, _apps);
            listView.ItemClick += ListOnItemClick;
            return view;
        }

        private void ListOnItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var intent = new Intent(Activity, typeof (AppActivity));
            intent.PutExtra("Id", _apps[e.Position].Id);
            //intent.PutExtra("Image", _apps[itemClickEventArgs.Position].Image);
            StartActivity(intent);
        }
    }
}