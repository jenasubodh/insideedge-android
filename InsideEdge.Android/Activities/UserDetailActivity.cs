using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.Adapters;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Activities
{
    [Activity(Label = "Back", Theme = "@style/Theme.Insideedge", Icon = "@android:color/transparent", ParentActivity = typeof(MainActivity))]
    [MetaData("android.support.PARENT_ACTIVITY", Value = "InsideEdge.Android.Activities.MainActivity")]

    public class UserDetailActivity : Activity
    {
        private UserViewModel user = Util.GenerateUser();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetHomeButtonEnabled(true);

            SetContentView(Resource.Layout.user_profile);
        }
    }
}