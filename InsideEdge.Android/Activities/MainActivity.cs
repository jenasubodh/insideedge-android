using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.Fragments;
using InsideEdge.Android.Helpers;
using Fragment = Android.Support.V4.App.Fragment;

namespace InsideEdge.Android.Activities
{
    [Activity(Label = "InsideEdge", Theme = "@style/Theme.Insideedge", Icon = "@android:color/transparent", LaunchMode = LaunchMode.SingleTop)]
    //[Activity(Label = "InsideEdge", Icon = "@android:color/transparent")]
    public class MainActivity : FragmentActivity
    {
        private static readonly string[] Sections = {"Dashboard", "Store", "Settings", "Help"};
        private DrawerLayout _mDrawer;
        private ListView _mDrawerList;
        private string _mDrawerTitle;
        private MyActionBarDrawerToggle _mDrawerToggle;
        private string _mTitle;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.main);
            _mTitle = _mDrawerTitle = Title;
            _mDrawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            _mDrawerList = FindViewById<ListView>(Resource.Id.left_drawer);
            _mDrawerList.Adapter = new ArrayAdapter<string>(this, Resource.Layout.item_menu, Sections);

            _mDrawerList.ItemClick += (sender, args) => ListItemClicked(args.Position);
            _mDrawer.SetDrawerShadow(Resource.Drawable.drawer_shadow_dark, (int) GravityFlags.Start);


            //DrawerToggle is the animation that happens with the indicator next to the actionbar
            _mDrawerToggle = new MyActionBarDrawerToggle(this, _mDrawer,
                Resource.Drawable.ic_drawer_light,
                Resource.String.drawer_open,
                Resource.String.drawer_close);

            //Display the current fragments title and update the options menu
            _mDrawerToggle.DrawerClosed += (o, args) =>
            {
                ActionBar.Title = _mTitle;
                InvalidateOptionsMenu();
            };

            //Display the drawer title and update the options menu
            _mDrawerToggle.DrawerOpened += (o, args) =>
            {
                ActionBar.Title = _mDrawerTitle;
                InvalidateOptionsMenu();
            };

            //Set the drawer lister to be the toggle.
            _mDrawer.SetDrawerListener(_mDrawerToggle);


            //if first time you will want to go ahead and click first item.
            if (bundle == null)
            {
                ListItemClicked(0);
            }

            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetHomeButtonEnabled(true);
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            _mDrawerToggle.SyncState();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            _mDrawerToggle.OnConfigurationChanged(newConfig);
        }


        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            bool drawerOpen = _mDrawer.IsDrawerOpen(_mDrawerList);
            //when open don't show anything
            for (int i = 0; i < menu.Size(); i++)
            {
                menu.GetItem(i).SetVisible(!drawerOpen);
            }
            return base.OnPrepareOptionsMenu(menu);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main_activity_actions, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        // Pass the event to ActionBarDrawerToggle, 
        //if it returns true, then it has handled the app icon touch event
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (_mDrawerToggle.OnOptionsItemSelected(item))
                return true;
            switch (item.ItemId)
            {
                case Resource.Id.action_user:
                    OpenProfile();
                    return true;
                case Resource.Id.action_update:
                    CheckUpdate();
                    return true;
                case Resource.Id.action_logout:
                    PerformLogout();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        private void PerformLogout()
        {
            Toast.MakeText(this, "Logging Out", ToastLength.Short).Show();
            StartActivity(typeof(LoginActivity));
        }

        private void CheckUpdate()
        {
            Toast.MakeText(this, "No Update Available at this Moment, Try again sometime later !!", ToastLength.Short).Show();
        }

        private void OpenProfile()
        {
            StartActivity(typeof(UserDetailActivity));
            //Toast.MakeText(this, "Open Profile is Not yet Implemented", ToastLength.Short).Show();
        }

        private void ListItemClicked(int position)
        {
            Fragment fragment = null;
            switch (position)
            {
                case 0:
                    fragment = new DashboardFragment();
                    break;
                case 1:
                    fragment = new AppViewPagerFragment();
                    break;
                case 2:
                    fragment = new SettingsFragment();
                    break;
                case 3:
                    fragment = new HelpFragment();
                    break;
            }


            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment).Commit();

            _mDrawerList.SetItemChecked(position, true);
            ActionBar.Title = _mTitle = Sections[position];
            _mDrawer.CloseDrawer(_mDrawerList);
        }
    }
}