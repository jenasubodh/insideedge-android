using Android.App;
using Android.OS;
using Android.Widget;

namespace InsideEdge.Android.Activities
{
    [Activity(Label = "Application", Icon = "@android:color/transparent")]
    public class AppActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.app_fragment);

            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetHomeButtonEnabled(true);

            string title = Intent.GetStringExtra("Title");

            Toast.MakeText(this, title, ToastLength.Long).Show();
        }
    }
}