using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace InsideEdge.Android.Activities
{
    [Activity(Label = "Login")]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestedOrientation = ScreenOrientation.Portrait;
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.login);

            var loginBtn = FindViewById<Button>(Resource.Id.signin);
            loginBtn.Click += (sender, e) => StartActivity(typeof (MainActivity));
        }
    }
}