using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.Adapters;
using InsideEdge.Android.ViewModels;

namespace InsideEdge.Android.Activities
{
    [Activity(Label = "Back", Theme = "@style/Theme.Insideedge", Icon = "@android:color/transparent", ParentActivity = typeof(MainActivity))]
    [MetaData("android.support.PARENT_ACTIVITY", Value = "InsideEdge.Android.Activities.MainActivity")]

    public class ApplicationDetailActivity : Activity
    {
        private readonly List<ApplicationViewModel> items = Util.GenerateApps();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            string data = Intent.GetStringExtra("Id");

            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetHomeButtonEnabled(true);

            SetContentView(Resource.Layout.appDescription);
            // Create your application here
            var imgView = (ImageView) FindViewById(Resource.Id.imageRelative);
            ApplicationViewModel item = items[int.Parse(data)];
            imgView.SetImageResource(item.Img);
            var appNameTextView = (TextView) FindViewById(Resource.Id.textView4);
            appNameTextView.Text = item.AppName;
            var platformTextView = (TextView) FindViewById(Resource.Id.textView2);
            platformTextView.Text = item.CategoryName;
            var releaseDatTextView = (TextView) FindViewById(Resource.Id.textView3);
            releaseDatTextView.Text = item.ReleaseDate;
            var versionTextView = (TextView) FindViewById(Resource.Id.textView6);
            versionTextView.Text = item.Version.ToString();
            var lastmodifiedDateTextView = (TextView) FindViewById(Resource.Id.textView7);
            lastmodifiedDateTextView.Text = item.LastModifiedDate;
            var numberofUsersTextView = (TextView) FindViewById(Resource.Id.textView8);
            numberofUsersTextView.Text = item.NumberOfUsers.ToString();
            var submitButton = (Button) FindViewById(Resource.Id.submitBtn);
            //Button submitButton = (Button)view.FindViewById(Resource.Id.submitBtn);
            //submitButton.Click += (o, e) => {
            //};
            var ratingBar = (RatingBar) FindViewById(Resource.Id.li_company_rating);
            submitButton.Click += delegate { submitButton.Text = ratingBar.Rating.ToString(); };
        }
    }
}