using System;

namespace InsideEdge.Android.ViewModels
{
    public class CategoriesViewModel
    {
        public long Id { get; set; }
        public String CategoryName { get; set; }
        public String CategoryIocn { get; set; }
        public String CategoryDesc { get; set; }
        public int AppCountCategoryWise { get; set; }
    }
}