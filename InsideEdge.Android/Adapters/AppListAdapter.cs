using System.Collections.Generic;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.ViewModels;
using Java.Lang;
using Android.Graphics;

namespace InsideEdge.Android.Adapters
{
    public class AppListAdapter : BaseAdapter
    {
        private readonly FragmentActivity _context;
        private readonly List<ApplicationViewModel> _items;

        public AppListAdapter(FragmentActivity context, List<ApplicationViewModel> items)
        {
            _context = context;
            _items = items;
        }

        //Return the number of List Items
        public override int Count
        {
            get { return _items.Count; }
        }

        //Return the object position
        public override long GetItemId(int position)
        {
            return position;
        }

        //Returns the selected option respective obejct
        public override Object GetItem(int position)
        {
            return null;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ApplicationViewModel item = _items[position];
            // re-use an existing view, if one is available
            View view = convertView;
            // otherwise create a new one
            if (view == null)
            {
                view = _context.LayoutInflater.Inflate(Resource.Layout.applist_list, null);
            }
            view.FindViewById<TextView>(Resource.Id.app_Name).Text = item.AppName;
            view.FindViewById<ImageView>(Resource.Id.app_Icon).SetImageResource(item.Img);
            view.FindViewById<TextView>(Resource.Id.app_Desc).Text = item.CategoryName;
            view.FindViewById<TextView>(Resource.Id.app_Platform).Text = item.Platform;
            view.FindViewById<RatingBar>(Resource.Id.myRatingBar).Rating = item.Rating;

            //Disable install button if already installed
            if(!item.Installed)
            {
                var btn = view.FindViewById<Button>(Resource.Id.downloadBtn);
                btn.SetCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn.Text = "INSTALLED";
                btn.SetTextColor(Color.ParseColor("#009933"));
                btn.Enabled = false;
            }
            
           
            return view;
        }
    }
}