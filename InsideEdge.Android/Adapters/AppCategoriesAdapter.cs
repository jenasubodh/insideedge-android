using System.Collections.Generic;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using InsideEdge.Android.ViewModels;
using Java.Lang;

namespace InsideEdge.Android.Adapters
{
    public class AppCategoriesAdapter : BaseAdapter
    {
        private readonly FragmentActivity _context;
        private readonly List<CategoriesViewModel> _items;

        public AppCategoriesAdapter(FragmentActivity context, List<CategoriesViewModel> items)
        {
            _context = context;
            _items = items;
        }

        //Return the number of List Items
        public override int Count
        {
            get { return _items.Count; }
        }

        //Return the object position
        public override long GetItemId(int position)
        {
            return position;
        }

        //Returns the selected option respective obejct
        public override Object GetItem(int position)
        {
            return null;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            CategoriesViewModel item = _items[position];
            // re-use an existing view, if one is available
            View view = convertView;
            // otherwise create a new one
            if (view == null)
            {
                view = _context.LayoutInflater.Inflate(Resource.Layout.app_category_list, null);
            }
            view.FindViewById<TextView>(Resource.Id.app_Category_Name).Text = item.CategoryName;
            view.FindViewById<TextView>(Resource.Id.app_Category_Desc).Text = item.CategoryDesc;
            view.FindViewById<TextView>(Resource.Id.app_Category_Counts).Text = item.AppCountCategoryWise + " Apps";
            return view;
        }
    }
}