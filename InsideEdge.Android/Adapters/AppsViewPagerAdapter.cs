using Android.Support.V4.App;
using InsideEdge.Android.Fragments;
using Java.Lang;

namespace InsideEdge.Android.Adapters
{
    internal class AppsViewPagerAdapter : FragmentPagerAdapter
    {
        private static readonly string[] Content =
        {
            "Categories", "All Apps", "Top Apps", "My Apps"
        };

        public AppsViewPagerAdapter(FragmentManager p0) : base(p0)
        {
        }

        public override int Count
        {
            get { return Content.Length; }
        }

        public override Fragment GetItem(int index)
        {
            switch (index)
            {
                case 0:
                    return new AppCategoriesFragment();
                case 1:
                    return new AllAppsFragments();
                case 2:
                    return new TopAppsFragments();
                case 3:
                    return new MyAppsFragment();
            }

            return null;
        }

        public override ICharSequence GetPageTitleFormatted(int p0)
        {
            return new String(Content[p0%Content.Length].ToUpper());
        }
    }
}